var http = require("http"),
    path=require("path"),
    fs = require('fs'),
    Router=require("router");
    var router = new Router();
    



var server=http.createServer(function (request, response) {
    router( request, response, function( error ) {
        if ( !error ) {
          response.writeHead( 404 );
        } else {
          // Handle errors
          console.log( error.message, error.stack );
          response.writeHead( 400 );
        }
        response.end( 'RESTful API Server is running!' );
    });
}).listen(process.env.PORT);

router.get('/js/:filename',printJavaScript)
router.get('/',indexHandler)
//api
router.get('/weather',printWeather)

//api end

// Console will print the message
console.log('Server running at http://0.0.0.0:8080/');

function indexHandler(request,response){
    printHtml('client/main.html',response)
}




function printHtml(path,response){
    fs.readFile(path, function (err, html) {
        if (err) {
            throw err; 
        } 
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(html)
        response.end()
    }); 
}

function printJavaScript(request,response){
    console.log(request.params.filename)
    path='client/js/'+request.params.filename
    fs.readFile(path, function (err, script) {
        if (err) {
            throw err; 
        } 
        response.writeHead(200, {'Content-Type': 'text/javascript'});
        response.write(script)
        response.end()
    }); 
}
function getFilename(path){
    var startIndex = (path.indexOf('\\') >= 0 ? path.lastIndexOf('\\') : path.lastIndexOf('/'));
    var filename = path.substring(startIndex);
    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
        filename = filename.substring(1);
    }
    return filename
}



function printWeather(request,response){
    var url="http://api.openweathermap.org/data/2.5/forecast/daily?id=1819729&mode=json&units=metric&cnt=7&APPID=3cdd97dd0435da8f07c8533b596abb6f"
    var api_request = http.get(url, function (api_response) {
                    
        console.log("in api_request")

        var buffer = "", 
            weather_data,
            route;
    
        api_response.on("data", function (chunk) {
            buffer += chunk;
        });

        api_response.on("end", function (err) {

            console.log(buffer);
            console.log("\n");
            weather_data = JSON.parse(buffer);
            response.writeHead(200, {'Content-Type': 'application/json'} );

            response.write(JSON.stringify(weather_data))
            response.end()
        });
    })
}